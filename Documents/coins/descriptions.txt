a
Name: 1947 MAPLE LEAF CANADA 10-CENT VERY FINE (VF-20)
Description: 1947 Canada Maple Leaf 10-cent Very Fine (VF-20)

b
Name: 1859 CANADA 1-CENT ICCS CERTIFIED VG-10 NARROW 9; BRASS; CORROSION
Description: 1859 Canada 1-Cent ICCS Certified VG-10 Narrow 9; Brass; Corrosion

c
Name: 2019 CANADA LOUIS RIEL: FATHER OF MANITOBA SPECIAL EDITION PROOF SILVER DOLLAR (NO TAX)

Description: 
2019 Canada Louis Riel: Father of Manitoba - Li Payr Di Manitoba Special Edition Proof Silver Dollar (No Tax)

First appearance on a Canadian coin. This is the first Canadian legal tender coin to feature Métis Nation leader Louis Riel (1844-1885), and the first to feature the Michif language.
Celebrate Métis history and culture through art. From the looped form of the Métis sash to the beaded coat, this inspiring design seamlessly fuses art, symbolism and history. This is the first collaboration between the Royal Canadian Mint and artist David Garneau.
A favourite with collectors, the Special Edition Proof Dollar is one of our most popular annual offerings.

Specifications:
Item Number: 174637
Composition: 99.99% pure silver
Mintage: 15,000
Weight*: 23.17 g
Diameter: 36.07 mm
Face Value: $1
Finish: Proof
Edge: Serrated
Artist: David Garneau
Packaging: Black clamshell with black beauty box



