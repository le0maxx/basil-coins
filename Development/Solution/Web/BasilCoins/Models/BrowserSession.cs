﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasilCoins.Models
{
    public class BrowserSession
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public int ID_User { get; set; }
        public int ID_Customer { get; set; }

    }
}
